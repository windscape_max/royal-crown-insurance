//
//  RCIPhotoAttachmentCollectionViewCell.swift
//  Royal Crown Insurance
//
//  Created by Admin on 11.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

protocol RCIPhotoAttachmentDelegate: class {
    func deleteButtonPressed(indexPath: IndexPath)
    
}

class RCIPhotoAttachmentCollectionViewCell: UICollectionViewCell {

    var indexPath: IndexPath!
    weak var delegate: RCIPhotoAttachmentDelegate?
    
    @IBOutlet weak var deletePhotoButton: UIButton!
    @IBOutlet weak var photoAttachment: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        self.photoAttachment.image = nil
    }

    @IBAction func deletePhotoAction(_ sender: UIButton) {
        delegate?.deleteButtonPressed(indexPath: indexPath)
    }

}
