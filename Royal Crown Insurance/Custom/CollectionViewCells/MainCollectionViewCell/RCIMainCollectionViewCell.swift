//
//  RCIMainCollectionViewCell.swift
//  Royal Crown Insurance
//
//  Created by Admin on 08.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class RCIMainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var modulTitle: UILabel!
    @IBOutlet weak var moduleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        self.modulTitle.text = ""
        self.moduleImage.image = nil
    }
}
