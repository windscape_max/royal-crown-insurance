//
//  RCIQuizListTableViewCell.swift
//  Royal Crown Insurance
//
//  Created by Admin on 12.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

protocol RCIQuizListTableViewCellDelegate: class {
    func cellTapped(row: Int)
}

class RCIQuizListTableViewCell: UITableViewCell {

    @IBOutlet weak var quizTitle: UILabel!
    @IBOutlet weak var quizDescription: UILabel!
    
    weak var delegate: RCIQuizListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        quizTitle.text = ""
        quizDescription.text = ""
    }
}
