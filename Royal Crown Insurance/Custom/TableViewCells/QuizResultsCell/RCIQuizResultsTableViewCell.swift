//
//  RCIQuizResultsTableViewCell.swift
//  Royal Crown Insurance
//
//  Created by Admin on 14.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

protocol QuizResultDelegate: class {
    func reviewAnswersPressed()
    func tryAgainPressed()
}

class RCIQuizResultsTableViewCell: UITableViewCell {

    @IBOutlet weak var resultScore: UILabel!
    @IBOutlet weak var resultDescription: UILabel!
    
    @IBOutlet weak var reviewAnswersButtonHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var reviewAnswersButton: UIButton!
    @IBOutlet weak var tryAgainButton: UIButton!
    
    weak var delegate: QuizResultDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        reviewAnswersButton.layer.cornerRadius = 3
        
        tryAgainButton.layer.cornerRadius = 3
        tryAgainButton.layer.borderColor = commonPurpleColor.cgColor
        tryAgainButton.layer.borderWidth = 0.5
    }

    override func prepareForReuse() {
        resultScore.text = ""
        resultDescription.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func reviewAnswersActiom(_ sender: UIButton) {
        delegate?.reviewAnswersPressed()
    }
    
    @IBAction func tryAgainAction(_ sender: UIButton) {
        delegate?.tryAgainPressed()
    }
}
