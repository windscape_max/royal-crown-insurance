//
//  RCIListTableViewCell.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class RCIListTableViewCell: UITableViewCell {

    @IBOutlet weak var itemTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        self.itemTitle.text = ""
    }
}
