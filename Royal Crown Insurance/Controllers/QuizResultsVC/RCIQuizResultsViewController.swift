//
//  RCIQuizResultsViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 14.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let quizCell = "quizResultsCell"
private let quizList = "quizListCellIdentificator"

protocol QuizResultsVCDelegate: class {
    func tryAgain()
}

class RCIQuizResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, QuizResultDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var message = ""
    var score = ""
    
    var questionsArray: [RCIQuestionsModel] = []
    var answers: [RCIAnswersModel] = []
    
    var reviewAnswers = false
    
    weak var delegate: QuizResultsVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UINib.init(nibName: "RCIQuizResultsTableViewCell", bundle: nil), forCellReuseIdentifier: quizCell)
        self.tableView.register(UINib.init(nibName: "RCIQuizListTableViewCell", bundle: nil), forCellReuseIdentifier: quizList)
        self.tableView.estimatedRowHeight = 250
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
 
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if reviewAnswers {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if reviewAnswers {
            if section == 0 {
                return questionsArray.count
            } else {
                return 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if reviewAnswers {
            
            if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: quizList, for: indexPath) as! RCIQuizListTableViewCell
                
                cell.quizTitle.text = "\(indexPath.row+1)/\(questionsArray.count)\nQ: \(questionsArray[indexPath.row].text)"
                if answers[indexPath.row].correct == true {
                    cell.quizDescription.textColor = UIColor.lightGray
                } else {
                    cell.quizDescription.textColor = UIColor.red
                }
                cell.quizDescription.font = cell.quizDescription.font.withSize(13)
                cell.quizDescription.text = "A: \(answers[indexPath.row].text)"
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: quizCell, for: indexPath) as! RCIQuizResultsTableViewCell
                cell.delegate = self
                cell.backgroundColor = UIColor.groupTableViewBackground
                
                cell.resultScore.frame.size = CGSize(width: 0, height: 0)
                cell.resultScore.text = ""
                cell.resultDescription.frame.size = CGSize(width: 0, height: 0)
                cell.resultDescription.text = ""
                
                cell.reviewAnswersButton.setTitle("", for: .normal)
                
                cell.reviewAnswersButtonHeightConstraint.constant = 0
                
                cell.tryAgainButton.frame.size = CGSize(width: 150, height: 40)
                cell.tryAgainButton.backgroundColor = commonPurpleColor
                cell.tryAgainButton.setTitleColor(UIColor.white, for: .normal)
                
                return cell
            }
            
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: quizCell, for: indexPath) as! RCIQuizResultsTableViewCell
            cell.delegate = self
            cell.resultScore.text = "Your result: \(score)"
            cell.resultDescription.attributedText = message.htmlAttributedString(fontSize: 20)
            
            return cell
        }
    }
    
    // MARK: - QuizResultDelegate
    
    func reviewAnswersPressed() {
        reviewAnswers = true
        self.tableView.reloadData()
    }
    
    func tryAgainPressed() {
        delegate?.tryAgain()
    }
}

extension String {
    func htmlAttributedString(fontSize: CGFloat = 17.0) -> NSAttributedString? {
        let fontName = UIFont.systemFont(ofSize: fontSize).fontName
        let string = self.appending(String(format: "<style>body{font-family: '%@'; font-size:%fpx;}</style>", fontName, fontSize))
        guard let data = string.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        
        return try! NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
    }
}
