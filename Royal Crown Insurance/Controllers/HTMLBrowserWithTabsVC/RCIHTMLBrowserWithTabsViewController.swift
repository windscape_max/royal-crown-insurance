//
//  RCIHTMLBrowserWithTabsViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class RCIHTMLBrowserWithTabsViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var firstWebView: UIWebView!
    
    var model: RCIWhatToDoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        segmentedControl.layer.cornerRadius = 0
        segmentedControl.layer.borderWidth = 0.5
        segmentedControl.layer.masksToBounds = true
        
        if let model = model {
            self.navigationItem.title = model.title
            
            segmentedControl.setTitle(model.tab_1_title, forSegmentAt: 0)
            segmentedControl.setTitle(model.tab_2_title, forSegmentAt: 1)
            segmentedControl.selectedSegmentIndex = 0
            
            
            firstWebView.loadHTMLString("<html><body style=\"background-color: white; color: #151B54\"\(model.tab_1_content)</body></html>", baseURL: nil)
            firstWebView.scrollView.bounces = false
            firstWebView.backgroundColor = UIColor.clear

            segmentedControl.selectedSegmentIndex = 0
            
            let segAttributesSelected: NSDictionary = [NSForegroundColorAttributeName: UIColor.white,
                                                       NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!]
            let segAttributesNormal: NSDictionary = [NSForegroundColorAttributeName: commonPurpleColor,
                                                     NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!]
            
            segmentedControl.setTitleTextAttributes(segAttributesSelected as [NSObject : AnyObject], for: .selected)
            segmentedControl.setTitleTextAttributes(segAttributesNormal as [NSObject : AnyObject], for: .normal)
        }
    }

    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
 
    // FINISH THIS
    private func changeSegmentUI() {
        for i in segmentedControl.subviews {
            if let myControl = i as? UIControl {
                if myControl.isSelected {
                    myControl.tintColor = commonPurpleColor
                } else {
                    myControl.tintColor = UIColor.white
                }
            }
        }
        let segAttributesSelected: NSDictionary = [NSForegroundColorAttributeName: UIColor.white,
                                                   NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!]
        let segAttributesNormal: NSDictionary = [NSForegroundColorAttributeName: commonPurpleColor,
                                                 NSFontAttributeName: UIFont(name: "Helvetica Neue", size: 17)!]
        
        segmentedControl.setTitleTextAttributes(segAttributesSelected as [NSObject : AnyObject], for: .selected)
        segmentedControl.setTitleTextAttributes(segAttributesNormal as [NSObject : AnyObject], for: .normal)
    }
    
    @IBAction func segmentControlValueChanged(_ sender: UISegmentedControl) {
        guard let model = model else { return }
        changeSegmentUI()
        if sender.selectedSegmentIndex == 0 {
            firstWebView.loadHTMLString("<html><body style=\"background-color: white; color: #151B54\"\(model.tab_1_content)</body></html>", baseURL: nil)
        } else {
            firstWebView.loadHTMLString("<html><body style=\"background-color: white; color: #151B54\"\(model.tab_2_content)</body></html>", baseURL: nil)
        }
    }
    
    // MARK: - UIWebView Delegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.openURL(request.url!)
            return false
        }
        return true
    }
}
