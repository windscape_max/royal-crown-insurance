//
//  RCIListTableViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let reusableIdentificator = "listCellReusableIdentificator"

class RCIListTableViewController: UITableViewController {

    var vcTitle = ""
    
    private var servicesArray: [RCIServicesModel] = []
    private var whatToDoArray: [RCIWhatToDoModel] = []
    
    private var page = 1
    private var totalCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setup() {
        self.navigationItem.title = vcTitle
        
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.register(UINib.init(nibName: "RCIListTableViewCell", bundle: nil), forCellReuseIdentifier: reusableIdentificator)
    }
    
    private func loadData() {
        weak var weakSelf = self
        switch vcTitle {
        case "Business":
            RCINetworkService.instance.getServicesList(page: page, perPage: 10, type: .business, completion: { (response: [RCIServicesModel]) in
                weakSelf?.totalCount = response.count
                for i in response {
                    weakSelf?.servicesArray.append(i)
                }
                //weakSelf?.servicesArray = response
                weakSelf?.tableView.reloadData()
            })
            break
        case "Personal":
            RCINetworkService.instance.getServicesList(page: page, perPage: 10, type: .personal, completion: { (response: [RCIServicesModel]) in
                weakSelf?.totalCount = response.count
                for i in response {
                    weakSelf?.servicesArray.append(i)
                }
                //weakSelf?.servicesArray = response
                weakSelf?.tableView.reloadData()
            })
            break
        case "What To Do If":
            RCINetworkService.instance.getWhatToDoList(page: page, perPage: 10, completion: { (response: [RCIWhatToDoModel]) in
                weakSelf?.totalCount = response.count
                for i in response {
                    weakSelf?.whatToDoArray.append(i)
                }
                //weakSelf?.whatToDoArray = response
                weakSelf?.tableView.reloadData()
            })
            break
        default:
            break
        }
    }
    
    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch vcTitle {
        case "Business":
            return servicesArray.count
        case "Personal":
            return servicesArray.count
        case "What To Do If":
            return whatToDoArray.count
        default:
            return 0
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentificator, for: indexPath) as! RCIListTableViewCell

        switch vcTitle {
        case "Business":
            cell.itemTitle.text = servicesArray[indexPath.row].title
            
            if indexPath.row == servicesArray.count - 1 {
                if totalCount != 0 {
                    page += 1
                    loadData()
                }
            }
        case "Personal":
            cell.itemTitle.text = servicesArray[indexPath.row].title
            
            if indexPath.row == servicesArray.count - 1 {
                if totalCount != 0 {
                    page += 1
                    loadData()
                }
            }
        case "What To Do If":
            cell.itemTitle.text = whatToDoArray[indexPath.row].title
            
            if indexPath.row == whatToDoArray.count - 1 {
                if totalCount != 0 {
                    page += 1
                    loadData()
                }
            }
        default:
            break
        }
        
        return cell
    }

    // MARK: - Table View Delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch vcTitle {
        case "Business":
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "htmlBrowserVC") as! RCIHTMLBrowserViewController
            vc.vcTitle = servicesArray[indexPath.row].title
            vc.htmlString = servicesArray[indexPath.row].description
            vc.webSiteLink = servicesArray[indexPath.row].website
            vc.isWebsiteButtonHiden = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "Personal":
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "htmlBrowserVC") as! RCIHTMLBrowserViewController
            vc.vcTitle = servicesArray[indexPath.row].title
            vc.htmlString = servicesArray[indexPath.row].description
            vc.webSiteLink = servicesArray[indexPath.row].website
            vc.isWebsiteButtonHiden = false
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case "What To Do If":
            if whatToDoArray[indexPath.row].tabs == true {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "htmlWithTabsVC") as! RCIHTMLBrowserWithTabsViewController
                vc.model = whatToDoArray[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "htmlBrowserVC") as! RCIHTMLBrowserViewController
                vc.vcTitle = whatToDoArray[indexPath.row].tab_1_title
                vc.htmlString = whatToDoArray[indexPath.row].tab_1_content
                vc.isWebsiteButtonHiden = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            break
        default:
            break
        }
    }
    
}
