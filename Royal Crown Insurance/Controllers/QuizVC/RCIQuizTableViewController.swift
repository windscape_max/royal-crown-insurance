//
//  RCIQuizTableViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let reuseIdentificator = "quizListCellIdentificator"

class RCIQuizTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private var quizList: [RCIQuestionariesModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        RCINetworkService.instance.getQuestionariesList { (response) in
            self.quizList = response
            self.tableView.reloadData()
        }        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setup() {             
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        self.navigationItem.title = "Quiz"
        
        self.tableView.register(UINib.init(nibName: "RCIQuizListTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentificator)
        self.tableView.tableFooterView = UIView(frame: .zero)
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizList.isEmpty ? 0 : quizList.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentificator, for: indexPath) as! RCIQuizListTableViewCell
        
        cell.quizTitle.text = quizList[indexPath.row].title
        cell.quizDescription.text = quizList[indexPath.row].description

        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "questionsVC") as! RCIQuestionsPageViewController
        vc.navigationItem.title = quizList[indexPath.row].title
        vc.quizID = quizList[indexPath.row].id
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
