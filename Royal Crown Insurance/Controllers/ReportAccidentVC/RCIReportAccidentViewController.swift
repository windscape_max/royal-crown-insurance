//
//  RCIReportAccidentViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

protocol ScrollViewCustomDelegate: class {
    func touchesBegan()
}

private let reuseId = "photoAttachmentReuseIdentifier"
var imageArray: [UIImage] = []

class RCIReportAccidentViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, RCIPhotoAttachmentDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!

    private lazy var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var carReg: UITextField!
    @IBOutlet weak var telNumber: UITextField!
    @IBOutlet weak var agreeSwitch: UISwitch!
    @IBOutlet weak var reportButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    deinit {
        imageArray = []
    }
    
    // MARK: - Custom functions
    
    private func setup() {
        self.navigationItem.title = "Accident Report"
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        self.reportButton.layer.cornerRadius = 16
        
        self.collectionView.register(UINib.init(nibName: "RCIPhotoAttachmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseId)
        
        scrollView.keyboardDismissMode = .onDrag
        
        name.delegate = self
        carReg.delegate = self
        telNumber.delegate = self
    }
    
    @objc private func goToMain() {
        weak var weakSelf = self
        let alert = UIAlertController(title: "Close report accident", message: "Are you sure you want ot close report accident?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes!", style: .default) { (UIAlertAction) in
            weakSelf?.navigationController?.popToRootViewController(animated: true)
        }
        let no = UIAlertAction(title: "No!", style: .cancel, handler: nil)
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func getImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alert = UIAlertController(title: "", message: "Do you want open camera or library?", preferredStyle: .actionSheet)
            
            let openCameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (UIAlertAction) in
                self.openCamera()
            })
            let openLibraryAction = UIAlertAction(title: "Library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: { (UIAlertAction) in
                self.dismiss(animated: true, completion: nil)
            })
            
            alert.addAction(openCameraAction)
            alert.addAction(openLibraryAction)
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
        } else if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let alert = UIAlertController(title: "Camera isn't available!", message: "Sorry, camera isn't avalible. Do u want open library?", preferredStyle: .alert)
            let openLibraryAction = UIAlertAction(title: "Open library", style: .default, handler: { (UIAlertAction) in
                self.openLibrary()
            })
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            
            alert.addAction(cancel)
            alert.addAction(openLibraryAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func openLibrary() {
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func openCamera() {
        imagePicker.delegate = self
        imagePicker.sourceType = .camera;
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    private func isFormValid() -> Bool {
        
        if name.text == "" {
            showError(withTitle: "Empty Name", text: "Please, type your name.")
            return false
        }
        
        if carReg.text == "" {
            showError(withTitle: "Empty Registration Nymber", text: "Please, type your Registration Number.")
            return false
        }
        
        if telNumber.text == "" {
            showError(withTitle: "Empty Phone Number", text: "Please, type your Phone Number.")
            return false
        }
        
        if imageArray.count == 0 {
            showError(withTitle: "Attach Image", text: "Please, attach some photos.")
            return false
        }
        
        if !agreeSwitch.isOn {
            showError(withTitle: "Agreement", text: "Please, confirm that you are agree to procees your data.")
            return false
        }
        
        return true
    }
    
    private func showError(withTitle title: String, text: String) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK!", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == name {
            carReg.becomeFirstResponder()
        } else if textField == carReg {
            telNumber.becomeFirstResponder()
        } else if textField == telNumber {
            telNumber.resignFirstResponder()
        }
        
        return true
    }
    
    // MARK: - Actions
    
    @IBAction func reportAccidentAction(_ sender: UIButton) {
        if isFormValid() {
            let model = RCISubmitAccidentReportModel(name: name.text!, policy_number: carReg.text!, phone_number: telNumber.text!)
            weak var weakSelf = self
            RCINetworkService.instance.submitAccidentReport(model: model, imagesArray: imageArray, completion: { 
                weakSelf?.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    // MARK: - UIImagePickerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageArray.append(image)
            self.collectionView.reloadData()
        }
        
        self.dismiss(animated: true, completion: nil);
    }
    
    // MARK: - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return imageArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath) as! RCIPhotoAttachmentCollectionViewCell
        
        if indexPath.section == 0 {
            cell.photoAttachment.image = #imageLiteral(resourceName: "add_photo_icon")
            cell.deletePhotoButton.isHidden = true
        }else {
            cell.photoAttachment.image = imageArray[indexPath.row]
            cell.indexPath = indexPath
            cell.delegate = self
            cell.deletePhotoButton.isHidden = false
        }
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 && indexPath.row == 0 {
            getImage()
        }
    }
    
    // MARK: - RCIPhotoAttachmentDelegate
    
    func deleteButtonPressed(indexPath: IndexPath) {
        imageArray.remove(at: indexPath.row)
        self.collectionView.deleteItems(at: [indexPath])
        self.collectionView.reloadData()
        self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
    }
}

extension UIScrollView {
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for i in self.subviews {
            if i is UITextField {
                i.resignFirstResponder()
            }
        }
    }
}
