//
//  RCIMainCollectionViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 08.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let reuseIdentifier = "mainCollectionViewCellIdentificator"
private let modulesPictures = ["royal_assist_image", "royal_payment_image", "services_image", "what_to_do_if_image", "about_image", "questionnaries_image"]
private let modulesTitles = ["ROYAL ASSIST", "ROYAL PAYMENT", "SERVICES", "WHAT TO DO IF", "ABOUT", "QUIZ"]

class RCIMainCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setup() {
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "main_logo"))
        self.collectionView!.register(UINib.init(nibName: "RCIMainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return modulesPictures.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RCIMainCollectionViewCell
        
        let moduleImage = UIImage(named: modulesPictures[indexPath.row])
        
        cell.modulTitle.text = modulesTitles[indexPath.row]
        cell.moduleImage.image = moduleImage
        
        return cell
    }

    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.row {
        case 2:
            return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height * 0.33)
        case 5:
            return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height * 0.33)
        default:
            return CGSize(width: collectionView.bounds.width / 2 - 5, height: collectionView.bounds.height * 0.33)
        }
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "royalAssistVC"))!, animated: true)
            break
        case 1:
            guard let url = URL(string: "https://www.jccsmart.com/eBills/Welcome/Index/9634031") else { return }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            break
        case 2:
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "servicesVC"))!, animated: true)
            break
        case 3:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "listTableVC") as! RCIListTableViewController
            vc.vcTitle = "What To Do If"
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 4:
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "aboutVC"))!, animated: true)
            break
        case 5:
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "quizVC"))!, animated: true)
            break
        default:
            return
        }
    }
}
