//
//  RCIBranchesViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit
import CoreLocation
import Foundation
import GoogleMaps

class RCIBranchesViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate  {

    let locationManager = CLLocationManager()
    
    @IBOutlet weak var infoBottomView: UIView!
    @IBOutlet weak var googleMapView: GMSMapView!
    
    @IBOutlet weak var officeTitle: UILabel!
    @IBOutlet weak var officeAddress: UILabel!
    @IBOutlet weak var officeFax: UILabel!
    @IBOutlet weak var officePhone: UILabel!
    @IBOutlet weak var officeEmail: UILabel!
    @IBOutlet weak var officeLatitude: UILabel!
    @IBOutlet weak var officeLongtitude: UILabel!
    private var isViewFull = true
    
    private var modelsArray: [RCIBranchesModel] = []
    
    private var selectedMarker: GMSMarker?
    private var markerID = -1
    
    private var prevHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        loadBranches()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prevHeight = self.officeFax.frame.height
    }
    
    // MARK: - Custom methods
    private func loadBranches() {
        weak var weakSelf = self
        RCINetworkService.instance.getBranchesList { (response) in
            weakSelf?.modelsArray = response
            weakSelf?.setMarkers()
        }
    }
    
    private func setMarkers() {
        
        for i in modelsArray {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: i.latitude, longitude: i.longitude)
            marker.title = i.title
            marker.icon = UIImage(named: "pin_passive_icon")
            marker.map = googleMapView
            
            if selectedMarker == nil {
                markerID = i.id
                selectedMarker = marker
                selectedMarker?.icon = UIImage(named: "pin_active_icon")
                fillMarkerInfoView(marker: selectedMarker!)
                let camera = GMSCameraPosition.camera(withLatitude: (selectedMarker?.position.latitude)!, longitude: (selectedMarker?.position.longitude)!, zoom: 7.0)
                googleMapView.camera = camera
            }
            if markerID == i.id {
                selectedMarker = marker
                selectedMarker?.icon = UIImage(named: "pin_active_icon")
                fillMarkerInfoView(marker: selectedMarker!)
            }
        }
    }
    
    private func setup() {
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        }
        
        self.navigationItem.title = "Branches"
        
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem

        googleMapView.isMyLocationEnabled = true
        googleMapView.bringSubview(toFront: infoBottomView)
        
    }
    
    private func fillMarkerInfoView(marker: GMSMarker) {
        for i in modelsArray {
            if i.title == marker.title {
                markerID = i.id
                officeTitle.text = i.title
                officeAddress.text = i.address
                officeFax.text = "Fax: \(i.fax)"
                officePhone.text = "Postal code & phone: \(i.postal_code) \(i.phone)"
                officeEmail.text = "E: \(i.email)"
                officeLatitude.text = "T: \(i.latitude!)"
                officeLongtitude.text = "F: \(i.longitude!)"
            }
        }
        self.view.layoutIfNeeded()
    }
    
    private func showError(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func drawRoute(startLocation: CLLocationCoordinate2D) {
        let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(startLocation.latitude),\(startLocation.longitude)&destination=\(selectedMarker!.position.latitude),\(selectedMarker!.position.longitude)&sensor=false&mode=driving")!
        
        self.googleMapView.clear()
        setMarkers()
        URLSession.shared.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if(error != nil){
                self.showError(error: (error?.localizedDescription)!)
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray

                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3
                            polyline.map = self.googleMapView
                        }
                    })
                }catch let error as NSError{
                    self.showError(error: error.localizedDescription)
                }
            }
        }).resume()
    }
    
    // MARK: - Actions
    @IBAction func createRouteAction(_ sender: Any) {
        locationManager.startUpdatingLocation()
    }

    @IBAction func collapseViewAction(_ sender: UIButton) {
        sender.isEnabled = false

        if isViewFull {
            UIView.animate(withDuration: 0.7, animations: {
                
                self.officeFax.text = ""
                self.officePhone.text = ""
                self.officeEmail.text = ""
                self.officeLatitude.text = ""
                self.officeLongtitude.text = ""
                
                sender.setImage(UIImage.init(named: "arrow_up_icon"), for: .normal)
                self.isViewFull = false
                sender.isEnabled = true
                self.infoBottomView.layoutIfNeeded()
            })
        } else {
            
            UIView.animate(withDuration: 0.7, animations: {
                self.fillMarkerInfoView(marker: self.selectedMarker!)
                sender.setImage(UIImage.init(named: "arrow_down_icon"), for: .normal)
                self.isViewFull = true
                sender.isEnabled = true
                self.infoBottomView.layoutIfNeeded()
            })
        }
    }
    
    // MARK: - GMSViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if selectedMarker != marker {
            selectedMarker?.icon = UIImage(named: "pin_passive_icon")
            selectedMarker = marker
            selectedMarker?.icon = UIImage(named: "pin_active_icon")
            fillMarkerInfoView(marker: marker)
        }
        return true
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.drawRoute(startLocation: locValue)
        
        locationManager.stopUpdatingLocation()
    }
}
