//
//  RCIQuestionViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 13.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

protocol QuestionVCDelegate: class {
    func buttonNextPressed()
    func buttonPrevPressed()
    func isAnswerChoosed() -> Bool
    func cellPressed(answer: RCIAnswersModel)
    func shouldSelect(row: Int) -> Bool
}

private let reuseIdent = "quizListCellIdentificator"

class RCIQuestionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var model: RCIQuestionsModel?
    var totalQuestionsCount = 0
    var currentQuestion = 0
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    weak var delegate: QuestionVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib.init(nibName: "RCIQuizListTableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdent)
        
        let tblViewFooter = UIView(frame: .zero)
        tblViewFooter.backgroundColor = UIColor.groupTableViewBackground
        
        self.tableView.tableFooterView = tblViewFooter
        self.tableView.estimatedRowHeight = 150
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.pageControl.numberOfPages = totalQuestionsCount
        self.pageControl.currentPage = currentQuestion
    }

    // MARK: - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return (model?.answers.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdent, for: indexPath) as! RCIQuizListTableViewCell
        
        if indexPath.section == 0 {
            cell.contentView.backgroundColor = UIColor.groupTableViewBackground
            cell.quizTitle.text = "\n\(currentQuestion+1)/\(totalQuestionsCount)\n\(model!.text)"
            cell.quizTitle.textAlignment = .center
            cell.quizTitle.font = cell.quizTitle.font.withSize(17)
            cell.quizDescription.text = ""
        }else {
            if (delegate?.shouldSelect(row: indexPath.row))! {
                cell.quizDescription.textColor = UIColor.black
            }
            cell.quizTitle.text = ""
            cell.quizDescription.text = model?.answers[indexPath.row].text
            cell.quizDescription.font = cell.quizDescription.font.withSize(14)
        }

        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            delegate?.cellPressed(answer: (model?.answers[indexPath.row])!)
        }
    }
    
    // MARK: - Actions
    @IBAction func prevButtonPressed(_ sender: UIButton) {
        delegate?.buttonPrevPressed()
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if (delegate?.isAnswerChoosed())! {
            delegate?.buttonNextPressed()
        }else {
            let alert = UIAlertController(title: "Choose your answer", message: "You must choose answer!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
