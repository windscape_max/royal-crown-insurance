//
//  RCIQuestionsPageViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 13.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class RCIQuestionsPageViewController: UIPageViewController, UIPageViewControllerDelegate, QuestionVCDelegate, QuizResultsVCDelegate {

    var myVC: RCIQuestionViewController!
    
    private var questionsArray: [RCIQuestionsModel] = []
    private var answers: [RCIAnswersModel] = []
    private var currentQuestion = 0
    var quizID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        RCINetworkService.instance.getQuestionsListByQuestionarie(id: quizID) { (response) in
            self.questionsArray = response
            self.myVC = self.storyboard?.instantiateViewController(withIdentifier: "questionVC") as? RCIQuestionViewController
            self.myVC.model = self.questionsArray[self.currentQuestion]
            self.myVC.delegate = self
            self.myVC.totalQuestionsCount = self.questionsArray.count
            self.myVC.currentQuestion = self.currentQuestion
            self.setViewControllers([self.myVC], direction: .forward, animated: true, completion: nil)
        }
    }
    
    @objc private func goToMain(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func formModel() -> RCISubmitQuizModel {
        var answers: [RCIAnswersAttribute] = []
        
        for i in 0...questionsArray.count-1 {
            let answer = RCIAnswersAttribute(question_id: questionsArray[i].id, answer_id: self.answers[i].id)
            answers.append(answer)
        }
        let model = RCISubmitQuizModel(questionnaire_id: quizID, queAttributes: answers)
        
        return model
    }
    // NARK: - QuizResultsVCDelegate
    
    func tryAgain() {
        answers = []
        currentQuestion = 0
        myVC = self.storyboard?.instantiateViewController(withIdentifier: "questionVC") as? RCIQuestionViewController
        myVC.model = self.questionsArray[self.currentQuestion]
        myVC.delegate = self
        myVC.totalQuestionsCount = self.questionsArray.count
        myVC.currentQuestion = self.currentQuestion
        setViewControllers([self.myVC], direction: .reverse, animated: true, completion: nil)
    }
    
    
    // MARK: - QuestionVCDelegate
    
    func shouldSelect(row: Int) -> Bool {
        if currentQuestion < answers.count {
            if answers[currentQuestion].order == row {
                return true
            }
        }
        
        return false
    }
    
    func isAnswerChoosed() -> Bool {
        if currentQuestion < answers.count {
            return true
        }
        return false
    }
    
    func cellPressed(answer: RCIAnswersModel) {
        if currentQuestion == answers.count {
            answers.append(answer)
            self.buttonNextPressed()
            return
        }
        answers[currentQuestion] = answer
        self.buttonNextPressed()
    }
    
    func buttonPrevPressed() {
        currentQuestion -= 1
        if currentQuestion < 0 {
            currentQuestion = 0
        } else {
            self.myVC = self.storyboard?.instantiateViewController(withIdentifier: "questionVC") as? RCIQuestionViewController
            self.myVC.delegate = self
            self.myVC.model = self.questionsArray[currentQuestion]
            self.myVC.totalQuestionsCount = self.questionsArray.count
            self.myVC.currentQuestion = currentQuestion
            setViewControllers([myVC], direction: .reverse, animated: true, completion: nil)
        }
    }
    
    func buttonNextPressed() {
        currentQuestion += 1
        if currentQuestion >= questionsArray.count {
            myVC.tableView.isUserInteractionEnabled = false
            weak var weakSelf = self
            RCINetworkService.instance.submitQuiz(answers: formModel(), completion: { (message, score) in
                weakSelf?.currentQuestion = 0
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "resultsVC") as! RCIQuizResultsViewController
                vc.message = message
                vc.score = score
                vc.questionsArray = (weakSelf?.questionsArray)!
                vc.answers = (weakSelf?.answers)!
                vc.delegate = self
                
                weakSelf?.myVC.tableView.isUserInteractionEnabled = true
                
                weakSelf?.setViewControllers([vc], direction: .reverse, animated: true, completion: nil)
            })
        } else {
            self.myVC = self.storyboard?.instantiateViewController(withIdentifier: "questionVC") as? RCIQuestionViewController
            self.myVC.delegate = self
            self.myVC.model = self.questionsArray[currentQuestion]
            self.myVC.totalQuestionsCount = self.questionsArray.count
            self.myVC.currentQuestion = currentQuestion
            setViewControllers([myVC], direction: .forward, animated: true, completion: nil)
        }
    }
}
