//
//  RCIHTMLBrowserViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

class RCIHTMLBrowserViewController: UIViewController, UIWebViewDelegate {

    var vcTitle = ""
    var htmlString = ""
    var isWebsiteButtonHiden = false
    var webSiteLink = ""
    @IBOutlet weak var goToWebsiteButton: UIButton!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
        webView.loadHTMLString("<html><body style=\"background-color: white; color: #151B54\"<p>\(htmlString)</p></body></html>", baseURL: nil)
    }

    private func setup() {
        self.webView.scrollView.bounces = false
        self.goToWebsiteButton.isHidden = isWebsiteButtonHiden
        self.navigationItem.title = vcTitle
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }    

    @IBAction func openWebsite(_ sender: UIButton) {
        guard let url = URL(string: webSiteLink) else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    // MARK: - UIWebView Delegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.openURL(request.url!)
            return false
        }
        return true
    }
}
