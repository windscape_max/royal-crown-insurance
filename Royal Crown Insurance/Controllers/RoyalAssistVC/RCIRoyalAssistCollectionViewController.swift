//
//  RCIRoyalAssistCollectionViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let reuseIdentifier = "mainCollectionViewCellIdentificator"
private let modulesPictures = ["royal_assist_image", "make_a_call_image", "about_image"]
private let modulesTitles = ["REPORT AN ACCIDENT", "MAKE A CALL", "ABOUT ROYAL ASSIST"]

class RCIRoyalAssistCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
   
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func setup() {
        self.navigationItem.title = "Royal Assist"

        self.collectionView!.register(UINib.init(nibName: "RCIMainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }

    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func makeACall() {
        guard let number = URL(string: "tel://" + "77777773") else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(number)
        } else {
            UIApplication.shared.openURL(number)
        }
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return modulesPictures.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RCIMainCollectionViewCell
    
        let moduleImage = UIImage(named: modulesPictures[indexPath.row])
        
        cell.modulTitle.text = modulesTitles[indexPath.row]
        cell.moduleImage.image = moduleImage
    
        return cell
    }

    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height * 0.33)
    }
    
    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard RCINetworkService.instance.isInternetAvailable() else  {
            let alert = UIAlertController(title: "Internet Connection", message: "Please, check your internet connection!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
        }
        switch indexPath.row {
        case 0:
            self.navigationController?.pushViewController((self.storyboard?.instantiateViewController(withIdentifier: "reportAccidentVC"))!, animated: true)
            break
        case 1:
            makeACall()
            break
        case 2:
            weak var weakSelf = self
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "htmlBrowserVC") as! RCIHTMLBrowserViewController
            vc.vcTitle = "About Royal Assist"
            vc.isWebsiteButtonHiden = true
            RCINetworkService.instance.aboutRoyalAssistInfo(completion: { (result) in
                vc.htmlString = result
                weakSelf?.navigationController?.pushViewController(vc, animated: true)
            })
            //self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            break
        }
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
