//
//  RCIAboutCollectionViewController.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import UIKit

private let reuseIdentifier = "mainCollectionViewCellIdentificator"
private let modulesPictures = ["about_us_image", "brances_image", "ensured_image"]
private let modulesTitles = ["ABOUT US", "BRANCHES", "E-NSURED"]

class RCIAboutCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setup() {
        self.navigationItem.title = "About"
        
        self.collectionView!.register(UINib.init(nibName: "RCIMainCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        let rightBarButtonItem = UIBarButtonItem(image: UIImage.init(named: "red_logo_icon"), style: .plain, target: self, action: #selector(goToMain))
        rightBarButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    @objc private func goToMain() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return modulesPictures.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! RCIMainCollectionViewCell
        
        let moduleImage = UIImage(named: modulesPictures[indexPath.row])
        
        cell.modulTitle.text = modulesTitles[indexPath.row]
        cell.moduleImage.image = moduleImage
        
        return cell
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height * 0.33)
    }
    
    // MARK: UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard RCINetworkService.instance.isInternetAvailable() else  {
            let alert = UIAlertController(title: "Internet Connection", message: "Please, check your internet connection!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        switch indexPath.row {
        case 0:
            weak var weakSelf = self
            let vc = (self.storyboard?.instantiateViewController(withIdentifier: "htmlBrowserVC"))! as! RCIHTMLBrowserViewController
            vc.vcTitle = "About Us"
            vc.isWebsiteButtonHiden = true
            RCINetworkService.instance.aboutUsInfo(completion: { (result) in
                vc.htmlString = result
                weakSelf?.navigationController?.pushViewController(vc, animated: true)
            })
            break
        case 1:
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "branchesVC") as! RCIBranchesViewController
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            guard let url = URL(string: "https://cw.royalcrowninsurance.eu/Login") else { return }
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
            break
        default:
            return
        }
    }
}
