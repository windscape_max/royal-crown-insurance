//
//  RCIQuestionsModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 12.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import ObjectMapper

class RCIQuestionsModel: Mappable {
    
    var id : Int!
    var text = ""
    var order : Int!
    var answers: [RCIAnswersModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        text                <- map["text"]
        order               <- map["order"]
        answers             <- map["answers"]
    }
}

class RCIAnswersModel: Mappable {
    
    var id : Int!
    var text = ""
    var correct : Bool!
    var order : Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        text                <- map["text"]
        correct             <- map["correct"]
        order               <- map["order"]
    }
}
