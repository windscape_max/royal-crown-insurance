//
//  RCIServicesModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import ObjectMapper

class RCIServicesModel: Mappable{
    
    var id : Int!
    var title = ""
    var website = ""
    var type = ""
    var description = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        title               <- map["title"]
        website             <- map["website"]
        type                <- map["type"]
        description         <- map["description"]
    }
}
