//
//  RCISubmitQuizModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 14.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation

import ObjectMapper

class RCISubmitQuizModel: Mappable {
    
    var questionnaire_id  : Int!
    var questionnaire_answers_attributes: [RCIAnswersAttribute] = []
    
    init(questionnaire_id: Int, queAttributes: [RCIAnswersAttribute]) {
        self.questionnaire_id = questionnaire_id
        self.questionnaire_answers_attributes = queAttributes
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        questionnaire_id                    <- map["questionnaire_id"]
        questionnaire_answers_attributes    <- map["questionnaire_answers_attributes"]
    }
}

class RCIAnswersAttribute: Mappable {
    
    var question_id: Int!
    var answer_id: Int!
    
    init(question_id: Int, answer_id: Int) {
        self.question_id = question_id
        self.answer_id = answer_id
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        question_id         <- map["question_id"]
        answer_id           <- map["answer_id"]
    }
}
