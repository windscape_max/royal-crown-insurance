//
//  RCIBranchesModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 11.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation

import ObjectMapper

class RCIBranchesModel: Mappable{
    
    var id : Int!
    var title = ""
    var address = ""
    var phone = ""
    var fax = ""
    var email = ""
    var postal_code = ""
    var latitude: Double!
    var longitude: Double!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id              <- map["id"]
        title           <- map["title"]
        address         <- map["address"]
        phone           <- map["phone"]
        fax             <- map["fax"]
        email           <- map["email"]
        postal_code     <- map["postal_code"]
        latitude        <- map["latitude"]
        longitude       <- map["longitude"]
    }
}
