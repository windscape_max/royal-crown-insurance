//
//  RCIQuestionariesModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 12.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation

import ObjectMapper

class RCIQuestionariesModel: Mappable{
    
    var id : Int!
    var title = ""
    var description = ""
    var created_at = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        title               <- map["title"]
        description         <- map["description"]
        created_at          <- map["created_at"]
    }
}
