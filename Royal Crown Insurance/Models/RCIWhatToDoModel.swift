//
//  RCIWhatToDoModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import ObjectMapper

class RCIWhatToDoModel: Mappable{
    
    var id : Int!
    var title = ""
    var tabs: Bool!
    var tab_1_title = ""
    var tab_1_content = ""
    var tab_2_title = ""
    var tab_2_content = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        title               <- map["title"]
        tabs                <- map["tabs"]
        tab_1_title         <- map["tab_1_title"]
        tab_1_content       <- map["tab_1_content"]
        title               <- map["title"]
        tab_2_title         <- map["tab_2_title"]
        tab_2_content       <- map["tab_2_content"]
    }
}

