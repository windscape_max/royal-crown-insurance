//
//  RCISubmitAccidentReportModel.swift
//  Royal Crown Insurance
//
//  Created by Admin on 14.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation

import ObjectMapper

class RCISubmitAccidentReportModel: Mappable {
    
    var name = ""
    var reg_policy_number = ""
    var phone_number = ""
    //var photos_attributes: [UIImage] = []
    
    init(name: String, policy_number: String, phone_number: String) {
        self.name = name
        self.reg_policy_number = policy_number
        self.phone_number = phone_number
        //self.photos_attributes = photos_attributes
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name                    <- map["name"]
        reg_policy_number       <- map["reg_policy_number"]
        phone_number            <- map["phone_number"]
        //photos_attributes       <- map["photos_attributes"]
    }
}
