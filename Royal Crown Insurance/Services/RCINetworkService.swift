//
//  RCINetworkService.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
import AlamofireObjectMapper
import SVProgressHUD

enum ServicesType {
    case business
    case personal
}

class RCINetworkService {

    static let instance = RCINetworkService()
    
    private init() {}
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func aboutRoyalAssistInfo(completion: @escaping (_ result: String) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(aboutRoyalAssist)")
        
        SVProgressHUD.show()
        Alamofire.request(requestURL).responseJSON { (response) in
            
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            
            guard let json = response.result.value as? [String : Any]  else { return }
            if let about = json["about_royal_assist"] {
                completion(about as! String)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func aboutUsInfo(completion: @escaping (_ result: String) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(aboutUs)")

        SVProgressHUD.show()
        Alamofire.request(requestURL).responseJSON { (response) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            
            guard let json = response.result.value as? [String : Any]  else { return }
            if let about = json["about_us"] {
                completion(about as! String)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func getServicesList(page: Int, perPage: Int, type: ServicesType, completion: @escaping ([RCIServicesModel]) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(services)?page=\(page)&per_page=\(perPage)&service_type=\(type)")
        
        var page = page
        var perPage = perPage
        
        if page <= 0 { page = 1}
        if perPage <= 0 { perPage = 20}
        
        SVProgressHUD.show()
        Alamofire.request(requestURL).responseArray { (response: DataResponse<[RCIServicesModel]>) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            completion(response.result.value!)
            SVProgressHUD.dismiss()
        }
    }
    
    func getWhatToDoList(page: Int, perPage: Int, completion: @escaping ([RCIWhatToDoModel]) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(accidentInstructions)?page=\(page)&per_page=\(perPage)")
        
        var page = page
        var perPage = perPage
        
        if page <= 0 { page = 1}
        if perPage <= 0 { perPage = 20}
        
        SVProgressHUD.show()
        Alamofire.request(requestURL).responseArray { (response: DataResponse<[RCIWhatToDoModel]>) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            completion(response.result.value!)
            SVProgressHUD.dismiss()
        }
    }
    
    func getBranchesList(completion: @escaping ([RCIBranchesModel]) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(branches)")

        SVProgressHUD.show()
        Alamofire.request(requestURL).responseArray { (response: DataResponse<[RCIBranchesModel]>) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            completion(response.result.value!)
            SVProgressHUD.dismiss()
        }
    }
    
    func getQuestionariesList(completion: @escaping ([RCIQuestionariesModel]) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(questionnaries)")
        
        SVProgressHUD.show()
        Alamofire.request(requestURL).responseArray { (response: DataResponse<[RCIQuestionariesModel]>) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            completion(response.result.value!)
            SVProgressHUD.dismiss()
        }
    }
    
    func getQuestionsListByQuestionarie(id: Int, completion: @escaping ([RCIQuestionsModel]) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(questionsList)?questionnaire_id=\(id)")
        
        SVProgressHUD.show()
        Alamofire.request(requestURL).responseArray { (response: DataResponse<[RCIQuestionsModel]>) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            completion(response.result.value!)
            SVProgressHUD.dismiss()
        }
    }
    
    func submitQuiz(answers: RCISubmitQuizModel, completion: @escaping (String, String) -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(submit)")
        
        let JSONString = answers.toJSON()
        
        SVProgressHUD.show()
        Alamofire.request(requestURL, method: HTTPMethod.post, parameters: JSONString, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            if response.error != nil {
                SVProgressHUD.showError(withStatus: response.error?.localizedDescription)
                return
            }
            
            guard let json = response.result.value as? [String : Any]  else { return }
            guard let message = json["message"] else { SVProgressHUD.dismiss(); return }
            guard let score = json["score"] else { SVProgressHUD.dismiss(); return }

            completion(message as! String, score as! String)
            SVProgressHUD.dismiss()
        }
    }
    
    func submitAccidentReport(model: RCISubmitAccidentReportModel, imagesArray: [UIImage], completion: @escaping () -> ()) {
        let requestURL = serverAdress.appending("\(v1Route)\(accidentReports)")

        let json = model.toJSON()
        
        SVProgressHUD.show()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for images in imageArray {
                if let imageData = UIImageJPEGRepresentation(images, 0.7) {
                    multipartFormData.append(imageData, withName: "photos_attributes")
                }
                
                for (key, value) in json {
                    if let value = value as? String {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }
            }
        }, to: requestURL) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    if response.result.isSuccess{
                        SVProgressHUD.dismiss()
                        completion()
                    }
                }
            case .failure(let encodingError):
                SVProgressHUD.showError(withStatus: encodingError.localizedDescription)
            }
        }
    }
}

