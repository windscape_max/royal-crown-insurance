//
//  RCINetworkConstants.swift
//  Royal Crown Insurance
//
//  Created by Admin on 09.08.17.
//  Copyright © 2017 windscape. All rights reserved.
//


// #MARK: COMMON URLS
public let serverAdress = "http://31.131.21.105:82/"
public let v1Route = "api/v1/"

// #MARK: BRANCHES URLS
public let branches = "branches"

// #MARK: WHAT TO DO IF URLS
public let accidentInstructions = "accident_instructions"

// #MARK: SERVICES URLS
public let services = "services"

// #MARK: ABOUT US URLS
public let aboutUs = "about_us"

// #MARK: ABOUT ROYAL ASSIST URLS
public let aboutRoyalAssist = "about_royal_assist"

// #MARK: ACCIDENT REPORTS URLS
public let accidentReports = "accident_reports"

// #MARK: QUIZ URLS
public let questionnaries = "questionnaires"
public let questionsList = "questions" // ID Require
public let submit = "questionnaire_results" // POST Request

// #MARK: CONTACT INFO URLS
public let contacts = "contacts"
